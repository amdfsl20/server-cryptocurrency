const Sequelize = require("sequelize");
const UserModel = require("./models/user");
const FavoriteModel = require("./models/favorite");

const sequelize = new Sequelize({
  database: process.env.DB_NAME,
  username: process.env.DB_USER,
  password: process.env.DB_PWD,
  host: process.env.DB_HOST,
  port: 5432,
  dialect: "postgres",
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
});

const User = UserModel(sequelize, Sequelize);
const Favorite = FavoriteModel(sequelize, Sequelize);

// Connect Databases
Favorite.hasMany(User, {
  foreignKey: "id",
});

module.exports = {
  sequelize,
  User,
  Favorite,
};
