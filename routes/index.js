const router = require("express").Router();
const userRoute = require("../routes/user");
const favoriteRoute = require("../routes/favorite");

const { authentication } = require("../middlewares/auth");
const errorHandler = require("../middlewares/errorHandler");

router.use("/user", userRoute);

router.use(authentication);

router.use("/favorite", favoriteRoute);

router.use(errorHandler);

module.exports = router;
