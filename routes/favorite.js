const FavotiteController = require("../controllers/favorite");
const router = require("express").Router();

router.get("/", FavotiteController.favoriteGet);
router.post("/", FavotiteController.favoritePost);
router.delete("/", FavotiteController.favoriteDelete);

module.exports = router;
