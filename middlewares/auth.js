const { validateJWT } = require("../helpers/jsonWebToken");
const { User } = require("../sequelize");

function authentication(req, res, next) {
  const access_token =
    req.headers.authorization && req.headers.authorization.split(" ")[1];

  if (access_token) {
    try {
      const payload = validateJWT(access_token);

      User.findByPk(payload.id)
        .then((user) => {
          if (user) {
            req.user = { id: user.id, email: user.email };
            next();
          } else {
            next({ statusCode: 401, message: "Invalid/Wrong JWT" });
          }
        })
        .catch((err) => {
          next({ statusCode: 500 });
        });
    } catch (err) {
      next({ statusCode: 401, message: "Invalid/Wrong JWT" });
    }
  } else {
    next({ statusCode: 401, message: "Please login first" });
  }
}

module.exports = {
  authentication,
};
