const { generateJWT } = require("../helpers/jsonWebToken");
const { passwordHash, comparePassword } = require("../helpers/passwordBcrypt");
const { User } = require("../sequelize");

class UserController {
  static userGet(req, res, next) {
    User.findAll()
      .then((user) => {
        user.forEach((e) => delete e.dataValues.password);
        res.status(200).json({ user });
      })
      .catch((err) => {
        console.log(err);
        next({ statusCode: 500 });
      });
  }
  static userGetOne(req, res, next) {
    User.findByPk(req.params.id)
      .then((user) => {
        if (user) {
          delete user.dataValues.password;
          res.status(200).json({ user });
        } else {
          next({ statusCode: 404 });
        }
      })
      .catch((err) => {
        next({ statusCode: 500 });
      });
  }
  static userPost(req, res, next) {
    const { email, password, name } = req.body;
    User.create({ email, password: passwordHash(password), name })
      .then((user) => {
        delete user.dataValues.password;
        res.status(201).json({ user });
      })
      .catch((err) => {
        if (
          err.name === "SequelizeUniqueConstraintError" ||
          err.name === "SequelizeValidationError"
        ) {
          err = err.errors.map((e) => e.message);
          res.status(400).json({ message: err });
        } else {
          console.log(err);
          next({ statusCode: 500 });
        }
      });
  }
  static userPut(req, res, next) {
    const { name, email, password } = req.body;
    User.update(
      { name, email, password },
      {
        where: {
          id: req.params.id,
        },
      }
    )
      .then((user) => {
        if (user[0]) {
          res.status(200).json({ name, email });
        } else {
          next({ statusCode: 404 });
        }
      })
      .catch((err) => {
        if (
          err.name === "SequelizeUniqueConstraintError" ||
          err.name === "SequelizeValidationError"
        ) {
          err = err.errors.map((e) => e.message);
          res.status(400).json({ message: err });
        } else {
          next({ statusCode: 500 });
        }
      });
  }
  static userDelete(req, res, next) {
    let entity;
    User.findByPk(req.params.id)
      .then((user) => {
        entity = user;
        return User.destroy({
          where: {
            id: req.params.id,
          },
        });
      })
      .then((user) => {
        if (entity) {
          res
            .status(200)
            .json({ message: `${entity.email} success to delete` });
        } else {
          next({ statusCode: 404 });
        }
      })
      .catch((err) => {
        next({ statusCode: 500 });
      });
  }

  static login(req, res, next) {
    const { email, password } = req.body;
    User.findOne({
      where: {
        email,
      },
    })
      .then((user) => {
        if (user) {
          if (comparePassword(password, user.password)) {
            const access_token = generateJWT({
              id: user.id,
              email: user.id,
            });
            res.status(200).json({ access_token });
          } else {
            next({ statusCode: 401, message: "Wrong Email/Password" });
          }
        } else {
          next({ statusCode: 401, message: "Wrong Email/Password" });
        }
      })
      .catch((err) => {
        console.log(err);
        next({ statusCode: 500 });
      });
  }
}

module.exports = UserController;
