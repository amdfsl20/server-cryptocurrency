const { Favorite } = require("../sequelize");

class FavoriteController {
  static favoriteGet(req, res, next) {
    Favorite.findAll({
      where: {
        idUser: req.user.id,
      },
    })
      .then((favorite) => {
        favorite.forEach((e) => delete e.dataValues.password);
        res.status(200).json({ favorite });
      })
      .catch((err) => {
        console.log(err);
        next({ statusCode: 500 });
      });
  }

  static favoritePost(req, res, next) {
    const { uuidCrypto } = req.body;
    Favorite.create({ idUser: req.user.id, uuidCrypto })
      .then((favorite) => {
        delete favorite.dataValues.password;
        res.status(201).json({ favorite });
      })
      .catch((err) => {
        if (
          err.name === "SequelizeUniqueConstraintError" ||
          err.name === "SequelizeValidationError"
        ) {
          err = err.errors.map((e) => e.message);
          res.status(400).json({ message: err });
        } else {
          console.log(err);
          next({ statusCode: 500 });
        }
      });
  }

  static favoriteDelete(req, res, next) {
    Favorite.destroy({
      where: {
        idUser: req.user.id,
        uuidCrypto: req.body.uuidCrypto,
      },
    })
      .then((favorite) => {
        if (favorite) {
          res
            .status(200)
            .json({ message: `${req.body.name} success to delete` });
        } else {
          next({ statusCode: 404 });
        }
      })
      .catch((err) => {
        console.log(err);
        next({ statusCode: 500 });
      });
  }
}

module.exports = FavoriteController;
