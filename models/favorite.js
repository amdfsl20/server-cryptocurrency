module.exports = (sequelize, type) => {
  return sequelize.define(
    "favorite",
    {
      id: {
        type: type.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      idUser: type.BIGINT,
      uuidCrypto: type.STRING,
      createdAt: {
        type: "TIMESTAMP",
        defaultValue: sequelize.NOW,
        allowNull: false,
      },
      updatedAt: {
        type: "TIMESTAMP",
      },
    },
    {
      timestamps: true,
      freezeTableName: true,
    }
  );
};
