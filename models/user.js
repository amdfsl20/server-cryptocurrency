module.exports = (sequelize, type) => {
  return sequelize.define(
    "user",
    {
      id: {
        type: type.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: type.STRING,
        allowNull: false,
      },
      email: {
        type: type.STRING,
        allowNull: false,
      },
      password: {
        type: type.STRING,
        allowNull: false,
      },
      photoProfile: {
        type: type.STRING,
      },
      createdAt: {
        type: "TIMESTAMP",
        defaultValue: sequelize.NOW,
        allowNull: false,
      },
      updatedAt: {
        type: "TIMESTAMP",
      },
    },
    {
      timestamps: true,
      freezeTableName: true,
    }
  );
};
